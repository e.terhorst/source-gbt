import random

import factory

from gbt_catalog.models.article import Article


class ArticleFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Article
        django_get_or_create = ('sku', )

    sku = factory.LazyAttribute(lambda _: random.randint(0, 9999))
    ean = factory.LazyAttribute(lambda _: '0{}'.format(random.randint(00000000000, 99999999999)))

    name = factory.Faker('name')
    description = factory.Faker('text')

    unit_type = 'cm'
    unit_quantity = 100
    unit_description = "Price per meter"

    unit_price = factory.Faker('pydecimal', min_value=1, max_value=100, right_digits=2)
