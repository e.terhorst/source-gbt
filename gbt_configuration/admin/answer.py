from django.contrib import admin
from django.db import models
from django_json_widget.widgets import JSONEditorWidget

from gbt_configuration.models.answer import Answer


class AnswersInline(admin.TabularInline):
    model = Answer
    formfield_overrides = {
        models.JSONField: {'widget': JSONEditorWidget},
    }

    def has_delete_permission(self, request, obj=None):
        return False
