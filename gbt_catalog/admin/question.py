from django.utils.translation import ugettext as _
from django.contrib import admin

from gbt_catalog.models.question import (
    Question,
    QuestionInputField,
    QuestionArticleChoiceField
)


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    # TODO: Reverse link to content object

    list_display = ('id', 'reference', 'content_type', 'object_id', )
    list_filter = ('content_type', )
    list_display_links = ('id', 'reference', )
    search_fields = ('reference', )
    readonly_fields = ('id', )
    ordering = ('reference',)

    fieldsets = (
        (_('Object info'), {
            'fields': ('content_type', 'object_id', )
        }),
        (_('Management context'), {
            'fields': ('reference', )
        }),
    )


@admin.register(QuestionInputField)
class QuestionInputFieldAdmin(admin.ModelAdmin):

    list_display = ('id', )
    search_fields = ('id', 'input_field', )
    readonly_fields = ('id', )
    ordering = ('id', )

    fieldsets = (
        (_('Input field defenition'), {
            'fields': ('input_field', )
        }),
    )


@admin.register(QuestionArticleChoiceField)
class QuestionArticleChoiceFieldAdmin(admin.ModelAdmin):

    list_display = ('id', 'article_choices_field', )
    search_fields = ('id', 'article_choices', )
    readonly_fields = ('id', )
    ordering = ('id', )

    fieldsets = (
        (_('Article choices'), {
            'fields': ('article_choices', )
        }),
    )

    @staticmethod
    def article_choices_field(obj):
        # TODO: Reverse link to object
        return [f'{choice.sku}: {choice.name}' for choice in obj.article_choices.all()]
