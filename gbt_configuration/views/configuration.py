from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from gbt_catalog.serializers.product import ProductQuestionSetSerializer
from gbt_configuration.models.answer import Answer
from gbt_configuration.models.configuration import Configuration
from gbt_configuration.services.exceptions import AlreadyCompletedConfigurationServiceError
from gbt_configuration.serializers.configurator import (
    ConfigurationCreateSerializer,
    ConfigurationListSerializer,
    ConfigurationRetrieveSerializer
)

import gbt_configuration.services.answer
import gbt_configuration.services.configuration
import gbt_configuration.services.price
import gbt_configuration.services.question_set


class ConfigurationViewSet(viewsets.ModelViewSet):

    queryset = Configuration.objects.all()
    http_method_names = ('get', 'post', )

    def get_queryset(self):
        qs = self.queryset
        user = self.request.user
        if not user.is_staff and not user.is_superuser:
            qs = qs.filter(user=user)
        return qs

    def get_serializer_class(self):
        serializer = ConfigurationListSerializer
        if self.action == 'retrieve':
            serializer = ConfigurationRetrieveSerializer
        if self.action == 'create':
            serializer = ConfigurationCreateSerializer
        return serializer

    def perform_create(self, serializer):
        serializer.save()
        configuration = serializer.instance

        answer_tree = gbt_configuration.services.answer.build_for_configuration(
            configuration=serializer.instance
        )
        Answer.objects.create(configuration=configuration, answers=answer_tree)

        return serializer

    @action(methods=['get'], detail=True, url_path='questions', url_name='questions')
    def questions(self, request, pk=None):
        configuration = self.get_object()
        question_sets = gbt_configuration.services.question_set.all_for_configuration(
            configuration=configuration
        )
        serializer = ProductQuestionSetSerializer(data=question_sets, many=True)
        serializer.is_valid()
        return Response({'question_sets': serializer.data})

    @action(methods=['get', 'post'], detail=True, url_path='answers', url_name='answers')
    def answers(self, request, pk=None):
        if self.request.method == 'GET':
            return self._get_answers()
        elif self.request.method == 'POST':
            return self._set_answers(data=request.data)

    def _get_answers(self):
        configuration = self.get_object()
        answers = configuration.answers.answers
        return Response(answers)

    def _set_answers(self, data):
        if 'question_set_reference' not in data:
            raise ValidationError("Body requires 'question_set_reference' field to be set")

        configuration = self.get_object()
        gbt_configuration.services.answer.handle_answers(
            configuration=configuration,
            qs_reference=data['question_set_reference'],
            answers_update=data['answers']
        )
        return Response(configuration.answers.answers)

    @action(methods=['post'], detail=True, url_path='complete', url_name='complete')
    def complete(self, request, pk=None):
        configuration = self.get_object()

        unanswered_questions = gbt_configuration.services.configuration.all_unanswered_questions(
            configuration=configuration
        )
        if unanswered_questions:
            raise ValidationError({
                "code": "unanswered_questions",
                "unanswered": unanswered_questions
            })

        try:
            gbt_configuration.services.configuration.complete(configuration=configuration)
        except AlreadyCompletedConfigurationServiceError as ex:
            raise ValidationError(ex)

        serializer = ConfigurationRetrieveSerializer(instance=configuration)
        return Response(serializer.data)

    @action(methods=['get'], detail=True, url_path='price', url_name='price')
    def price(self, request, pk=None):
        configuration = self.get_object()
        price = gbt_configuration.services.price.get_for_configuration(configuration=configuration)
        return Response({'price': str(price)})
