from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'gbt_users'
    verbose_name = 'users'
