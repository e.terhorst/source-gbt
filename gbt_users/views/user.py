from django.contrib.auth import get_user_model
from rest_framework import viewsets, permissions

from gbt_users.serializers.user import (
    UserRetrieveSerializer,
    UserCreateSerializer,
    UserListSerializer
)


class AllowAnonymousCreateAction(permissions.BasePermission):

    def has_permission(self, request, view):
        if view.action == 'create':
            return True
        return False


class UserViewSet(viewsets.ModelViewSet):

    permission_classes = [AllowAnonymousCreateAction]
    queryset = get_user_model().objects.all()
    http_method_names = ('get', 'post',)

    def get_queryset(self):
        user = self.request.user
        qs = self.queryset
        if not user.is_staff and not user.is_superuser:
            qs = qs.filter(pk=user.pk)
        return qs

    def get_serializer_class(self):
        serializer = UserListSerializer
        if self.action == 'retrieve':
            serializer = UserRetrieveSerializer
        if self.action == 'create':
            serializer = UserCreateSerializer
        return serializer
