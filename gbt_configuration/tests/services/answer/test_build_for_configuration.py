from django.test import tag

from gbt_configuration.tests.mixins.configuration import ConfigurationTestCaseMixin

from gbt_users.factories.users import ActiveUserFactory

import gbt_configuration.services.answer


@tag("integration")
class BuildForConfigurationIntegrationTestCase(ConfigurationTestCaseMixin):

    def setUp(self):
        super().setUp()
        self.user = ActiveUserFactory()

        self.add_question_build(self.DEFAULT_QUESTION_ARTICLE_CHOICE)
        self.add_question_build(self.DEFAULT_QUESTION_ARTICLE_LENGTH)
        self.add_question_set_build(self.DEFAULT_QUESTION_SET_ARTICLE_CHOICE)
        self.add_question_set_build(self.DEFAULT_QUESTION_SET_ARTICLE_LENGTH)
        self.add_product_build(self.DEFAULT_PRODUCT)
        self.add_configuration_build(self.DEFAULT_PRODUCT)

        self.build_all()

    def test_call(self):
        """
        Case: Service is called to construct an answer tree for given configuration.
        Expected: Answer tree contains all the elements that are expected.
        """

        result = gbt_configuration.services.answer.build_for_configuration(configuration=self.configuration)

        self.assertDictEqual(
            result,
            {
                'def_qs_article_choice': {
                    'def_q_article_choice': None
                },
                'def_qs_article_length': {
                    'def_q_article_length': None
                }
            }
        )
