from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from gbt_configuration.admin.answer import AnswersInline
from gbt_configuration.models import Configuration

import gbt_configuration.services.answer


@admin.register(Configuration)
class ConfigurationAdmin(admin.ModelAdmin):

    list_display = ('id', 'product', 'user', 'created', 'updated', 'is_completed', 'price')
    search_fields = ('id', 'product', 'user', )
    list_filter = ('is_completed', )
    readonly_fields = ('id', 'created', 'updated', 'is_completed', 'price',)
    ordering = ('id', )
    inlines = [AnswersInline]

    fieldsets = (
        (_('Configuration'), {
            'fields': ('user', 'product',)
        }),
        (_('Read only'), {
            'fields': readonly_fields,
        }),
    )

    def save_related(self, request, form, formsets, change):
        gbt_configuration.services.answer.update_answers_set(
            configuration=form.instance,
            answers=form.instance.answers.answers
        )
        return super().save_related(request=request, form=form, formsets=formsets, change=change)
