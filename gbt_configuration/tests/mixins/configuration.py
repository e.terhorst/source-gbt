from decimal import Decimal

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase

from gbt_catalog.factories import (
    ArticleFactory,
    ProductFactory,
    QuestionArticleChoiceFieldFactory,
    QuestionInputFieldFactory,
    QuestionSetFactory,
    ProductQuestionSetFactory,
    ProductModifier
)
from gbt_catalog.factories.modifiers import ModifierPriceQuestionArticleFactory
from gbt_catalog.models.modifier import ModifierPriceArticleQuestion, Modifier
from gbt_catalog.models.question import Question, QuestionToSet
from gbt_configuration.factories.configuration import ConfigurationFactory


class ConfigurationTestCaseMixinException(Exception):
    pass


class ConfigurationTestCaseMixin(TestCase):
    """
    Configuration entities consist out of multiple relations. This mixin should provide the ability
    to quickly setup the scenario before the related objects are actually created. The default build
    provides context data suitable for the most test scenarios.
    """

    DEFAULT_ARTICLE = 'def_article'
    DEFAULT_QUESTION_ARTICLE_CHOICE = 'def_q_article_choice'
    DEFAULT_QUESTION_ARTICLE_LENGTH = 'def_q_article_length'
    DEFAULT_QUESTION_SET_ARTICLE_CHOICE = 'def_qs_article_choice'
    DEFAULT_QUESTION_SET_ARTICLE_LENGTH = 'def_qs_article_length'
    DEFAULT_PRODUCT = 'def_product'
    DEFAULT_MODIFIER_PARTIAL = 'def_modifier_partial'
    DEFAULT_MODIFIER_FULL = 'def_modifier_full'

    def setUp(self):
        self.user = None
        self.articles = {}
        self.questions = {}
        self.question_sets = {}
        self.modifiers = {}
        self.products = {}
        self.configuration = None

        self.article_builds = {
            self.DEFAULT_ARTICLE: {
                'sku': '0001',
                'ean': '0000000000001',
                'name': 'Article',
                'description': "Article description",
                'unit_type': 'cm',
                'unit_quantity': 100,
                'unit_description': 'Price per 100 cm',
                'unit_price': Decimal('15.00')
            }
        }

        self.question_builds = {
            self.DEFAULT_QUESTION_ARTICLE_CHOICE: {
                'article_choice': [self.DEFAULT_ARTICLE]
            },
            self.DEFAULT_QUESTION_ARTICLE_LENGTH: {
                'input_field': {
                    'widget': {
                        'type': 'integer_field',
                        'label': 'Length',
                        'placeholder': 'Length',
                        'help_text': None
                    },
                    'validation': {
                        'min': 100,
                        'max': 200,
                        'required': True
                    }
                }
            }
        }

        self.question_set_builds = {
            self.DEFAULT_QUESTION_SET_ARTICLE_CHOICE: {
                'title': 'Choose article',
                'questions': [
                    self.DEFAULT_QUESTION_ARTICLE_CHOICE,
                ]
            },
            self.DEFAULT_QUESTION_SET_ARTICLE_LENGTH: {
                'title': 'Article length',
                'questions': [
                    self.DEFAULT_QUESTION_ARTICLE_LENGTH,
                ]
            },
        }

        self.modifier_builds = {
            self.DEFAULT_MODIFIER_PARTIAL: {
                'type': 'article_question_modifier',
                'article': self.DEFAULT_ARTICLE,
                'question_target': self.DEFAULT_QUESTION_ARTICLE_CHOICE,
                'question_trigger': self.DEFAULT_QUESTION_ARTICLE_LENGTH,
                'modifier_method': ModifierPriceArticleQuestion.METHOD_ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL
            },
            self.DEFAULT_MODIFIER_FULL: {
                'type': 'article_question_modifier',
                'article': self.DEFAULT_ARTICLE,
                'question_target': self.DEFAULT_QUESTION_ARTICLE_CHOICE,
                'question_trigger': self.DEFAULT_QUESTION_ARTICLE_LENGTH,
                'modifier_method': ModifierPriceArticleQuestion.METHOD_ANSWER_VALUE_TO_UNIT_TYPE_FULL
            }
        }

        self.product_builds = {
            self.DEFAULT_PRODUCT: {
                'name': 'Default product',
                'description': 'A default product',
                'is_visible': True,
                'question_sets': [
                    self.DEFAULT_QUESTION_SET_ARTICLE_CHOICE,
                    self.DEFAULT_QUESTION_SET_ARTICLE_LENGTH
                ],
                'modifiers': [
                    self.DEFAULT_MODIFIER_PARTIAL
                ]
            }
        }

        self.configuration_build = {
            'product': self.DEFAULT_PRODUCT
        }

        self.build = {
            'articles': {},
            'questions': {},
            'question_sets': {},
            'modifiers': {},
            'products': {},
            'configuration': {}
        }

    def add_article_build(self, ref, clone_as_ref=None, custom_context=None):
        """
        :param str ref: Key of predefined article build
        :param str clone_as_ref: Clone a predefined build as a new object
        :param dict custom_context: Custom field values to apply
        """

        if ref not in self.article_builds:
            raise ConfigurationTestCaseMixinException(
                f"Article '{ref}' does not exist in build tree. Available are: {self.article_builds.keys()}"
            )
        if ref in self.build['articles']:
            raise ConfigurationTestCaseMixinException(
                f"Article '{ref}' already exists in build tree. Consider using 'as_ref'"
            )

        article_build = self.article_builds[ref]
        if custom_context:
            article_build.update(**custom_context)
        if clone_as_ref:
            ref = clone_as_ref

        self.build['articles'].update({ref: article_build})

    def add_custom_article_build(self, ref, context):
        """
        :param str ref: Key of custom article build
        :param dict context: Field values to apply
        """
        if ref in self.build['articles']:
            raise ConfigurationTestCaseMixinException(
                f"Article '{ref}' already exists in article build tree."
            )
        self.build['articles'].update({ref: context})

    def add_question_build(self, ref, clone_as_ref=None):
        if ref not in self.question_builds:
            raise ConfigurationTestCaseMixinException(
                f"Question '{ref}' does not exist in question samples. Available are: {self.question_builds.keys()}"
            )
        question_build = self.question_builds[ref]
        if clone_as_ref:
            ref = clone_as_ref
        self.build['questions'].update({ref: question_build})

    def add_question_set_build(self, ref, clone_as_ref=None):
        if ref not in self.question_set_builds:
            raise ConfigurationTestCaseMixinException(
                f"QuestionSet '{ref}' does not exist in question-set samples. "
                f"Available are: {self.question_builds.keys()}"
            )
        question_set_build = self.question_set_builds[ref]
        if clone_as_ref:
            ref = clone_as_ref
        self.build['question_sets'].update({ref: question_set_build})

    def add_modifier_build(self, ref, clone_as_ref=None):
        if ref not in self.modifier_builds:
            raise ConfigurationTestCaseMixinException(
                f"Modifier '{ref}' does not exist in modifier samples. "
                f"Available are: {self.modifier_builds.keys()}"
            )
        modifier_build = self.modifier_builds[ref]
        if clone_as_ref:
            ref = clone_as_ref
        self.build['modifiers'].update({ref: modifier_build})

    def add_product_build(self, ref, clone_as_ref=None):
        if ref not in self.product_builds:
            raise ConfigurationTestCaseMixinException(
                f"Product '{ref}' does not exist in product samples. "
                f"Available are: {self.product_builds.keys()}"
            )
        product_build = self.product_builds[ref]
        if clone_as_ref:
            ref = clone_as_ref
        self.build['products'].update({ref: product_build})

    def add_configuration_build(self, product_ref):
        if product_ref not in self.product_builds:
            raise ConfigurationTestCaseMixinException(
                f"Product '{product_ref}' does not exist in product samples. "
                f"Available are: {self.product_builds.keys()}"
            )
        self.build['configuration'].update({
            'product': product_ref,
            'user': self.user
        })

    def build_all(self):
        self.build_articles()
        self.build_questions()
        self.build_question_sets()
        self.build_modifiers()
        self.build_products()
        self.build_configuration()

    def build_articles(self):
        for ref, article_build in self.build['articles'].items():
            self.articles.update({ref: ArticleFactory(**article_build)})

    def build_questions(self):
        for ref, build in self.build['questions'].items():
            question_type = list(build.keys())[0]
            question_build = build[question_type]

            if question_type == 'article_choice':
                articles = [article.name for ref, article in self.articles.items() if ref in question_build]
                question = QuestionArticleChoiceFieldFactory(article_choices=articles)
                self.questions.update({ref: question})

            elif question_type == 'input_field':
                question = QuestionInputFieldFactory(input_field=question_build)
                self.questions.update({ref: question})

            else:
                raise ConfigurationTestCaseMixinException(
                    f"No handling defined for creating question of type '{question_type}' (ref: {ref})"
                )

            content_type = ContentType.objects.get_for_model(model=question)
            Question.objects.get_or_create(
                reference=ref,
                content_type=content_type,
                object_id=question.id
            )

    def build_question_sets(self):
        for ref, question_set_build in self.build['question_sets'].items():
            questions = question_set_build.pop('questions')
            question_set = QuestionSetFactory(reference=ref, **question_set_build)

            for priority, question_ref in enumerate(questions):
                QuestionToSet.objects.create(
                    question_set=question_set,
                    question=self._get_base_question(question=self.questions[question_ref]),
                    priority=priority
                )

            self.question_sets.update({ref: question_set})

    def build_modifiers(self):
        for ref, modifier_build in self.build['modifiers'].items():
            modifier_type = modifier_build.pop('type')

            if modifier_type == 'article_question_modifier':
                article = self.articles[modifier_build['article']]

                question_target = self._get_base_question(self.questions[modifier_build['question_target']])
                question_trigger = self._get_base_question(self.questions[modifier_build['question_trigger']])
                modifier = ModifierPriceQuestionArticleFactory(
                    article=article,
                    question_target=question_target,
                    question_trigger=question_trigger
                )
            else:
                raise ConfigurationTestCaseMixinException(
                    f"No handling defined for creating modifier of type '{modifier_type}' (ref: {ref})"
                )
            content_type = ContentType.objects.get_for_model(model=modifier)
            Modifier.objects.create(reference=ref, content_type=content_type, object_id=modifier.id)

            self.modifiers.update({ref: modifier})

    def build_products(self):
        for ref, product_build in self.build['products'].items():
            question_set_references = product_build.pop('question_sets')
            modifier_references = product_build.pop('modifiers')
            product = ProductFactory(**product_build)

            for priority, question_set_reference in enumerate(question_set_references):
                ProductQuestionSetFactory(
                    product=product,
                    question_set__reference=question_set_reference,
                    priority=priority
                )

            modifiers = Modifier.objects.filter(reference__in=modifier_references)
            product_modifier = ProductModifier.objects.create(product=product)
            product_modifier.modifiers.set(modifiers)

            self.products.update({ref: product})

    def build_configuration(self):
        if not self.products:
            raise ConfigurationTestCaseMixinException(
                f"No product has been created, please build a product first"
            )
        product_reference = self.build['configuration']['product']
        if product_reference not in self.products:
            raise ConfigurationTestCaseMixinException(
                f"Product '{product_reference}' is not available. Options are: {[self.products.keys()]}"
            )
        if self.user is None:
            raise ConfigurationTestCaseMixinException(
                f"No user has been defined, please set 'self.user' in your test-case"
            )

        self.configuration = ConfigurationFactory(
            product=self.products[self.build['configuration']['product']],
            user=self.user
        )

    def _get_base_question(self, question):
        content_type = ContentType.objects.get_for_model(model=question)
        return Question.objects.get(content_type=content_type, object_id=question.id)

    def _get_base_modifier(self, modifier):
        content_type = ContentType.objects.get_for_model(model=modifier)
        return Modifier.objects.get(content_type=content_type, object_id=modifier.id)

    def _debug_build(self):
        from pprint import pprint
        pprint(self.build)
