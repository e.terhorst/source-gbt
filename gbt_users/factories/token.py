import factory
from rest_framework.authtoken.models import Token

from gbt_users.factories import UserFactory


class DRFTokenFactory(factory.django.DjangoModelFactory):
    """
    Factory for a DRF authentication token.
    """

    class Meta:
        model = Token
        django_get_or_create = ('user', )

    key = None  # Generated on save if left empty
    user = factory.SubFactory(UserFactory)
