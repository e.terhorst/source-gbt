from gbt_configuration.services.exceptions import AlreadyCompletedConfigurationServiceError


def all_unanswered_questions(configuration):
    """
    Simple crawler that finds all unanswered questions for the product configuration.
    """
    answers = configuration.answers.answers
    unanswered_questions = {}

    for qs_reference, question_sets in answers.items():
        for q_reference, answer in question_sets.items():
            if answer is None:
                if qs_reference not in unanswered_questions:
                    unanswered_questions[qs_reference] = []
                unanswered_questions[qs_reference].append(q_reference)

    return unanswered_questions


def complete(configuration):
    """
    Mark a configuration as final.
    """
    if configuration.is_completed:
        raise AlreadyCompletedConfigurationServiceError("Configuraton already completed")

    configuration.is_completed = True
    configuration.save()

    return configuration
