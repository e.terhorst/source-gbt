
def all_for_configuration(configuration):
    """
    Fetches the question_sets in order of priority for the given configurator's product.
    """

    question_sets = configuration\
        .product\
        .question_sets.order_by('priority')

    return question_sets
