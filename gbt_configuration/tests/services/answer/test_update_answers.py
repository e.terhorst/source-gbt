from decimal import Decimal

from django.test import tag

from gbt_configuration.factories.answer import AnswerFactory

from gbt_configuration.tests.mixins.configuration import ConfigurationTestCaseMixin
from gbt_users.factories.users import ActiveUserFactory

import gbt_configuration.services.answer


@tag('integration')
class UpdateAnswersIntegrationTestCase(ConfigurationTestCaseMixin):

    def setUp(self):
        super().setUp()
        self.user = ActiveUserFactory()

        self.add_article_build(self.DEFAULT_ARTICLE)
        self.add_question_build(self.DEFAULT_QUESTION_ARTICLE_CHOICE)
        self.add_question_build(self.DEFAULT_QUESTION_ARTICLE_LENGTH)
        self.add_question_set_build(self.DEFAULT_QUESTION_SET_ARTICLE_CHOICE)
        self.add_question_set_build(self.DEFAULT_QUESTION_SET_ARTICLE_LENGTH)
        self.add_modifier_build(self.DEFAULT_MODIFIER_PARTIAL)
        self.add_product_build(self.DEFAULT_PRODUCT)
        self.add_configuration_build(self.DEFAULT_PRODUCT)

        self.build_all()

        self.answers = AnswerFactory(
            configuration=self.configuration,
            answers={
                'def_qs_article_choice': {
                    'def_q_article_choice': None
                },
                'def_qs_article_length': {
                    'def_q_article_length': None
                }
            }
        )

    def test_update_answers_updates_configuration_answers(self):
        """
        Case: Service is called with answers for multiple question sets.
        Expected: Questions for configuration are properly updated.
        """

        new_answers = {
            'def_qs_article_choice': {
                'def_q_article_choice': '0001'
            },
            'def_qs_article_length': {
                'def_q_article_length': None
            }
        }

        result = gbt_configuration.services.answer.update_answers_set(
            configuration=self.configuration,
            answers=new_answers
        )

        self.assertDictEqual(result.answers.answers, new_answers)

    def test_update_answers_updates_configuration_price(self):
        """
        Case: Service is called with answers for multiple question sets, triggering price modifiers.
        Expected: Price modifiers are applied, price for configuration is updated.
        """

        new_answers = {
            'def_qs_article_choice': {
                'def_q_article_choice': '0001'
            },
            'def_qs_article_length': {
                'def_q_article_length': 200
            }
        }

        result = gbt_configuration.services.answer.update_answers_set(
            configuration=self.configuration,
            answers=new_answers
        )

        self.assertDictEqual(result.answers.answers, new_answers)
        self.assertEqual(result.price, Decimal('30.00'))
