from decimal import Decimal

from dynamic_fixtures.fixtures import BaseFixture

from gbt_catalog.factories import ArticleFactory


class Fixture(BaseFixture):

    def load(self):
        # Rail related articles
        ArticleFactory(
            sku='0001',
            ean='0000000000001',
            name='Rail',
            description='Een rails die aan het plafond dient te worden gemonteerd. Deze '
                        'rails bevatten runners waaraan de gordijnen kunnen worden opgehangen.',
            unit_type='cm',
            unit_quantity=100,
            unit_description='Prijs per meter',
            unit_price=Decimal('11.50')
        )

        # Rod related articles
        ArticleFactory(
            sku='0002',
            ean='0000000000002',
            name='Roede',
            description='Een roede is een dwarsstang die u tussen dragers of muren kunt monteren. '
                        'Aan de roede ringen om de dwarsstang kunnen de gordijnen eenvoudig worden '
                        'opgehangen.',
            unit_type='cm',
            unit_quantity=100,
            unit_description='Prijs per meter',
            unit_price=Decimal('10.00')
        )
