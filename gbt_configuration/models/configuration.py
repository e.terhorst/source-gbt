from decimal import Decimal

from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.db import models

from gbt_catalog.models.product import Product


class ConfigurationManager(models.Manager):

    def incomplete(self):
        return self.filter(is_completed=False)

    def complete(self):
        return self.filter(is_completed=True)


class Configuration(models.Model):
    """
    Description of a product configuration object.
    """

    objects = ConfigurationManager()

    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, db_index=True)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    is_completed = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=8, decimal_places=4, default=Decimal('0.00'))

    class Meta:
        db_table = 'gbt_configuration'
        verbose_name = _('Configuration')
        verbose_name_plural = _('Configurartions')
