from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _

from gbt_catalog.models.article import Article


class Question(models.Model):
    """
    The base of a question which is content-type bound to an other Question<Type>-object containing the appropriate
    structure to store the context in the database. This loosly coupling makes it possible to re-use Questions
    on different QuestionSets.

    Examples:
        - Question<QuestionInputField>: Describes how the input field should work
        - Question<QuestionArticleChoice>: Describes the available articles to choose from
    """

    reference = models.CharField(max_length=64, db_index=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        db_table = 'gbt_catalog_question'
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')

    def __str__(self):
        return f'{self.content_type}: {self.object_id} ({self.reference})'


class QuestionSet(models.Model):
    """
    The QuestionSet describes the general question to describe what we expect from the customer,
    the related Question objects contain the specific detail questions to configure the product.

    To give an example:
      - QuestionSet: "What are the dimension of the the room?" :: General question
      - Question<QuestionInputField> length in cm [<input>] :: specific question
      - Question<QuestionInputField> width in cm [<input>] :: specific question
      - Question<QuestionInputField> height in cm [<input>] :: specific question
    """

    reference = models.CharField(max_length=32)
    title = models.CharField(max_length=128)

    class Meta:
        db_table = 'gbt_catalog_question_set'
        verbose_name = _('Question set')
        verbose_name_plural = _('Question sets')

    def __str__(self):
        return f'{self.reference}: {self.title}'


class QuestionToSet(models.Model):
    """
    This relation table couples questions to question sets and defines the question's priority.

    Product[P_1]:
        QuestionSet[QS_12]: priority 1
            Question[Q_7]: priority 1 :: first
            Question[Q_3]: priority 2 :: second
        QuestionSet[QS_2]: priority 2
            Question[Q_128]: priority 1 :: first
            Question[Q_13]: priority 2 :: second
    """

    question = models.ForeignKey(Question, related_name='questions_from_set', on_delete=models.PROTECT)
    question_set = models.ForeignKey(QuestionSet, related_name='questions_from_set', on_delete=models.PROTECT)
    priority = models.PositiveIntegerField()

    class Meta:
        db_table = 'gbt_catalog_rel_question_questionset'
        ordering = ['priority', 'question_set']


class QuestionInputField(models.Model):
    """
    The QuestionInputField describes how the InputField should work.

    Example:
    {
        "type": "charField",
        "validators": {
            "starts_with": "ROOM_",
            "max_length": 128
        },
        "widget": {
            "type": "text",
            "placeholder": "ROOM_KITCHEN"
        }
    }
    """

    input_field = models.JSONField()

    class Meta:
        db_table = 'gbt_catalog_question_input_field'
        verbose_name = _('Question: InputField')
        verbose_name_plural = _('Questions: InputFields')

    def __str__(self):
        return f'QuestionInputField: {self.pk}'


class QuestionArticleChoiceField(models.Model):
    """
    The QuestionArticleChoice defines the base choices available to choose from.
    """

    # TODO: Reduce to 'choices'
    article_choices = models.ManyToManyField(Article, related_name='choices')

    class Meta:
        db_table = 'gbt_catalog_question_article_choice'
        verbose_name = _('Question: ArticleChoice')
        verbose_name_plural = _('Questions: ArticleChoices')
