from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken import views as auth_views

from gbt_configuration.views.configuration import ConfigurationViewSet
from gbt_users.views.user import UserViewSet
from gbt_catalog.views.product import ProductViewSet

router = routers.DefaultRouter()
router.register(r'user', UserViewSet, basename='users')
router.register(r'product', ProductViewSet, basename='products')
router.register(r'configuration', ConfigurationViewSet, basename='configurations')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include([
        path('auth/', auth_views.obtain_auth_token),
        path('', include(router.urls)),
    ])),
]
