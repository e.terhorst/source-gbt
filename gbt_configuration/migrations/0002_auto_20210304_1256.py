# Generated by Django 3.1.7 on 2021-03-04 12:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('gbt_configuration', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='answer',
            name='configuration',
            field=models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, related_name='answers', to='gbt_configuration.configuration'),
        ),
    ]
