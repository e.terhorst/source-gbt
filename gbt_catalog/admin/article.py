from django.utils.translation import ugettext as _
from django.contrib import admin

from gbt_catalog.models.article import Article


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):

    list_display = ('id', 'sku', 'ean', 'name', 'unit_type', 'unit_quantity', 'unit_price',)
    list_display_links = ('id', 'sku', )
    search_fields = ('sku', 'ean', 'name', 'unit')
    list_filter = ('unit_type', )
    ordering = ('sku',)

    fieldsets = (
        (_('Public context'), {
            'fields': ('name', 'description', 'price', 'unit_type', 'unit_quantity', 'unit_description', 'unit_price')
        }),
        (_('Management context'), {
            'fields': ('sku', 'ean', )
        }),
    )
