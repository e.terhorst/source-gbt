import factory
from django.contrib.auth import get_user_model


class UserFactory(factory.django.DjangoModelFactory):
    """
    Factory of a normal (inactive) user.
    """
    email = factory.Faker('email')
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')

    password = factory.PostGenerationMethodCall(
        method_name='set_password',
        raw_password='test2021'
    )

    is_active = True

    class Meta:
        model = get_user_model()
        django_get_or_create = ('email',)


class ActiveUserFactory(UserFactory):
    """
    Factory of an active user.
    """
    is_active = True


class StaffUserFactory(UserFactory):
    """
    Factory for active staff user.
    """
    is_active = True
    is_staff = True


class SuperUserFactory(StaffUserFactory):
    """
    Factory for active superuser.
    """
    is_superuser = True
