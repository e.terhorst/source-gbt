from dynamic_fixtures.fixtures import BaseFixture

from gbt_users.factories import SuperUserFactory, StaffUserFactory, UserFactory, DRFTokenFactory


class Fixture(BaseFixture):
    """
    Fixture for loading users in different states.
    """
    def load(self):
        # Inactive normal user
        DRFTokenFactory(
            key='5163d5f82e678c08d96ee79f2fe1b0bf1bd05f8f',
            user=UserFactory(
                email="inactive_user@edwinterhorst.nl",
                first_name='Inactive',
                last_name='User'
            )
        )
        # Active normal user
        DRFTokenFactory(
            key='cafaa2b87a9e7426280361f2d1ea07294ec70d1c',
            user=UserFactory(
                email="active_user@edwinterhorst.nl",
                is_active=True,
                first_name='Active',
                last_name='User'
            )
        )
        # Staff user
        DRFTokenFactory(
            key='0c7d1229f820aa34b73a3a906e4b094e8c84229f',
            user=StaffUserFactory(
                email="staffuser@edwinterhorst.nl",
                first_name="Staff",
                last_name="User"
            )
        )
        # Super user
        DRFTokenFactory(
            key='47e240d79f1908abe016befc72670b8c3014a2e8',
            user=SuperUserFactory(
                email="superuser@edwinterhorst.nl",
                first_name="Super",
                last_name="User"
            )
        )
