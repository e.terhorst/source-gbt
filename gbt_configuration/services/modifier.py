import gbt_configuration.services.price


def get_for_answers_update(configuration, answers_update):
    """
    Checks if there are questions in the new answer update that should trigger a modifier.

    # TODO: Not yet generic, only working for price modifiers > article/question

    :param gbt_configuration.models.Configuration configuration: Configuration instance.
    :param dict answers_update: The answers of the answered question set which need evaluation.

    :rtype: dict
    :return: dict containing information about the trigger, target and modifier for further handling.
    """

    product_modifiers = configuration.product.modifier_set.modifiers.all()
    modifiers_to_apply = []

    for product_modifier in product_modifiers:
        modifier = product_modifier.content_object

        q_trigger_ref = modifier.question_trigger.reference
        q_target_ref = modifier.question_target.reference

        # Scenario handler: User fills in the form and hits a modifier trigger question.
        # As for now, triggers handle a modifier for a question that has already been answered prior to this question.
        if q_trigger_ref in answers_update.keys():
            q_target_answer = _find_question_value(q_ref=q_target_ref, answers=configuration.answers.answers)
            if q_target_answer == modifier.article.sku:
                q_trigger_answer = _find_question_value(q_ref=q_trigger_ref, answers=answers_update)
                if q_trigger_answer is not None:
                    modifiers_to_apply.append({
                        'modifier': modifier,
                        'q_trigger_answer': q_trigger_answer,
                        'q_target_answer': q_target_answer
                    })

        # Scenario handler: User fills in the form and hits a target modifier by changing the original
        # answer. Currently this means an article change. Therefor we need to re-evaluate the price.
        if q_target_ref in answers_update.keys():
            q_trigger_answer = _find_question_value(q_ref=q_trigger_ref, answers=configuration.answers.answers)
            if q_trigger_answer:
                q_target_answer = _find_question_value(q_ref=modifier.question_target.reference,
                                                       answers=answers_update)
                if q_target_answer == modifier.article.sku:
                    modifiers_to_apply.append({
                        'modifier': modifier,
                        'q_trigger_answer': q_trigger_answer,
                        'q_target_answer': q_target_answer
                    })

    return modifiers_to_apply


def _find_question_value(q_ref, answers):
    """
    Find an answer from the answers set.

    :param str q_ref: Questions reference to search the answer for (needle)
    :param dict answers: List of answers to search in (haystack). This can be either answers of a single
                         questionset or multiple question-sets.

    :rtype: str | None
    :return: question's answer | None question answer could not be found or has not yet been answered.
    """

    if q_ref in answers.keys():
        return answers[q_ref]

    for key, subset in answers.items():
        if isinstance(subset, dict):
            if q_ref in subset.keys():
                return subset[q_ref]


def apply(configuration, modifiers_to_apply):
    """
    Apply given modifiers to the configuration.

    TODO: Currently only works for price modifiers. This service should seperate them in the future.
          For now it only calls the price modifier service for each modifier, since they are the only
          modifiers currently supported.

    :param gbt_configuration.models.configuration configuration: Configuration instance to apply the modifiers to.
    :param dict modifiers_to_apply: Modifiers context dict (see: get_for_answers_update)
    """

    modifier_prices = []
    for modifier_dict in modifiers_to_apply:
        modifier_prices.append(
            gbt_configuration.services.price.calculate_price_for_method(
                method=modifier_dict['modifier'].modifier_method,
                article=modifier_dict['modifier'].article,
                answer_value=int(modifier_dict['q_trigger_answer'])
            )
        )

    configuration.price = sum(modifier_prices)
    configuration.save()
