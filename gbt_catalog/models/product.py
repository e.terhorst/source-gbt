from django.db import models
from django.utils.translation import ugettext_lazy as _

from gbt_catalog.models.modifier import Modifier
from gbt_catalog.models.question import QuestionSet


class ProductManager(models.Manager):

    def visible(self):
        return self.filter(is_visible=True)


class Product(models.Model):
    """
    A Product object can be seen as the base blueprint for the product configuration.
    The configurator uses the related QuestionSets (coupled by ProductQuestionSet)
    handle the questions related to this product.
    """

    objects = ProductManager()

    reference = models.CharField(max_length=64)

    name = models.CharField(max_length=64)
    description = models.TextField()

    is_visible = models.BooleanField(default=False)

    class Meta:
        db_table = 'gbt_catalog_product'
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return f'{self.name} ({self.reference})'


class ProductQuestionSet(models.Model):
    """
    This relation table couples a question sets to a product and defines the question set's priority

    Product[P_1]:
        QuestionSet[QS_12]: priority 1 :: first
            Question[Q_7]: priority 1
            Question[Q_3]: priority 2
        QuestionSet[QS_2]: priority 2 :: second
            Question[Q_128]: priority 1
            Question[Q_13]: priority 2
    """

    product = models.ForeignKey(Product, related_name='question_sets', on_delete=models.PROTECT)
    question_set = models.ForeignKey(QuestionSet, on_delete=models.PROTECT)

    priority = models.PositiveIntegerField()

    class Meta:
        db_table = 'gbt_catalog_rel_product_questionset'
        ordering = ['priority', 'question_set']


class ProductModifier(models.Model):

    product = models.OneToOneField(Product, related_name='modifier_set', on_delete=models.PROTECT)
    modifiers = models.ManyToManyField(Modifier)
