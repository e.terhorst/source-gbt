import factory

from gbt_catalog.factories import ProductFactory
from gbt_configuration.models import Configuration
from gbt_users.factories import UserFactory


class ConfigurationFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Configuration
        django_get_or_create = ('user', 'product',)

    user = factory.SubFactory(UserFactory)
    product = factory.SubFactory(ProductFactory)

    is_completed = False
    price = 100
