from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    """
    User manager that overrides the creation of users so the user is registered
    with an email address instead of a username.
    """
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Factory of new user. Takes email as login instead of a username.

        :param str email: Email address to use.
        :param str password: Password to set.
        :param dict extra_fields: Optional model kwargs.

        :raises ValueError: Field email is not set properly

        :rtype: gbt_user.models.User
        :return: The newly created user object.
        """
        if not email:
            raise ValueError("The email address is required.")

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(raw_password=password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, **extra_fields):
        """
        Create a new normal user account. Takes email as login instead of a username.

        :param str email: Email address to use.
        :param str password: Password to set.
        :param dict extra_fields: Optional model kwargs.

        :rtype: gbt_user.models.User
        :return: The newly created user object.
        """
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email=email, password=password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """
        Create a new super user account. Takes email as login instead of a username.

        :param email: The email address to use.
        :param password: The password to set.
        :param extra_fields: Optional fields.

        :raises ValueError: - is_staff must be set to True
                            - is_superuser is not set to True

        :rtype: gbt_user.models.User
        :return: The newly created superuser object.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have attribute is_staff set to True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have attribute is_superuser set to True.')
        return self._create_user(email=email, password=password, **extra_fields)


class User(AbstractUser):
    """
    Representation of a user, uses an email address as login instead of a username.
    """
    objects = UserManager()

    username = None
    email = models.EmailField(_('E-mail address'), unique=True, db_index=True, error_messages={
        'unique': _("A user with this email address already exists.")
    })
    first_name = models.CharField(_('First name'), max_length=30)
    last_name = models.CharField(_('Last name'), max_length=50)

    is_active = models.BooleanField(
        _('Active'),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. Uncheck this instead of deleting accounts."
        )
    )

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    class Meta:
        db_table = 'gbt_user'
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        """
        :rtype: string
        :return: A user-friendly presentation of the instance.
        """
        return '{email} ({first_name} {last_name})'.format(
            email=self.email,
            first_name=self.first_name,
            last_name=self.last_name
        )

    def get_short_name(self):
        """
        :rtype: string
        :return: First name of the user.
        """
        return self.first_name

    def get_full_name(self):
        """
        :rtype: string
        :return: Full name of the user (first and last name).
        """
        return '{first_name} {last_name}'.format(
            first_name=self.first_name,
            last_name=self.last_name
        )
