from rest_framework import serializers

from gbt_catalog.models.product import Product
from gbt_catalog.serializers.product import ProductListSerializer
from gbt_configuration.models import Configuration


class ConfigurationListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Configuration
        fields = (
            'id',
            'user_id',
            'product_id',
            'is_completed'
        )


class ConfigurationRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = Configuration
        fields = (
            'id',
            'user_id',
            'product_id',
            'created',
            'updated',
            'is_completed'
        )


class ConfigurationCreateSerializer(serializers.ModelSerializer):

    product_reference = serializers.SlugRelatedField(
        queryset=Product.objects.all(),
        slug_field='reference',
        write_only=True
    )
    product = ProductListSerializer(read_only=True)
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = Configuration
        fields = (
            'product_reference',
            'product',
            'id'
        )

    def create(self, validated_data):
        return Configuration.objects.create(
            user=self.context.get('request').user,
            product=validated_data.get('product_reference')
        )
