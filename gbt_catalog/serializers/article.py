from rest_framework import serializers

from gbt_catalog.models.article import Article


class ArticleRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        fields = (
            'sku',
            'ean',
            'name',
            'description',
            'unit_price',
            'unit_description'
        )
