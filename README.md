Into The Source : Gordijn Bereken Tool
---

- Author: Edwin ter Horst
- Project: Gordijn Bereken Tool ([Source](https://gitlab.com/e.terhorst/source-gbt), [Issues](https://gitlab.com/e.terhorst/source-gbt/-/issues))
- Goal: Trial project for job-application
- Email: [contact@edwinterhorst.nl](mailto:contact@edwinterhorst.nl?subject=[GitLab]%20Gordijn%20Bereken%20Tool)


For the client (front-end), see:
- Gordijn Bereken Tool Cient ([Source](https://gitlab.com/e.terhorst/source-gbt-client), [Issues](https://gitlab.com/e.terhorst/source-gbt-client/-/issues))

# Running the application

##### Requirements
- [docker-compose](https://docs.docker.com/compose/install/)
- [git](https://git-scm.com/downloads)

##### Clone from GitLab
```
$ git clone https://gitlab.com/e.terhorst/source-gbt.git
```

##### Run Application
```
$ docker-compose up
```
- Waits for the database to be up and running, can take a few seconds;
- Automatically loads/updates the dynamic fixtures for a ready-to-go dev environment;

Links:
- Admin: http://localhost:8100/admin/
- API: http://localhost:8100/api/v1/

#### deafult user logins: 
 - `superuser@edwinterhorst.nl`
 - `staffuser@edwinterhorst.nl`
 - `active_user@edwinterhorst.nl`
 - `inactive_user@edwinterhorst.nl`

#### password for all
 - `test2021`

# TechStack

- [Git](https://www.git-scm.com/) - Source control manager
- [GitLab](https://www.gitlab.com/) - Online dev-ops tool
- [Docker](https://www.docker.com/) - Application containerization
- [Docker-Compose](https://docs.docker.com/compose/) - Composite multiple-container application
- [Poetry](https://python-poetry.org/) - Dependency orchestration
- [PostgreSQL](https://www.postgresql.org/) - Database management system
- [Python](https://www.python.org/) - My preferred programming language
- [Django](https://www.djangoproject.com/) - Python web-application framework
- [Django Extensions](https://github.com/django-extensions/django-extensions) A collection of custom extensions for django
- [Django Rest Framework](https://www.django-rest-framework.org/) Django REST API Framework
- [Django Cors Headers](https://pypi.org/project/django-cors-headers/) Django app to handle server headers required for Cross-Origin Resource Sharing (CORS)
- [Django Dynamic Fixtures](https://github.com/Peter-Slump/django-dynamic-fixtures) Django app to install dynamic fixtures
- [Factory Boy](https://factoryboy.readthedocs.io/en/stable/) Django app for easy-to-maintain model-based fixtures and data generation
- [Flake8](https://flake8.pycqa.org/en/latest/) Style guide enforcement

# Useful commands

##### Execute command on docker container
```
docker-compose exec <container> <command>

eg: 
- docker-compose exec db psql source_gbt --user gbt_postgres
- docker-compose exec app ./manage.py createsuperuser
- docker-compose exec app ./manage.py load_dynamic_fixtures
- docker-compose exec app ./manage.py shell_plus --ipython
- docker-compose exec app poetry <add/remove> <package>
- docker-compose exec app bash
```

# Django Rest Framework Authentication Token

##### Generating a user token
```
docker-compose exec app ./manage.py drf_create_token <user email>
```
> `Generated token 47e240d79f1908abe016befc72670b8c3014a2e8 for user contact@edwinterhorst.nl`

##### Curl an endpoint with a token-authorization header
```
curl -X GET http://localhost:8100/api/v1/users/ -H 'Authorization: Token <user token>'
```

# Base project design
A simple catalog builder based on questions. By defing question-sets, questions, articles, etc. you build a
catalog, which can be seen as building blocks for a configurable product. A visible product is available for
the customer to start a new configuration for it. All the questions that require an answer are exposed by the
ReST API client and should be presented in the front-end client to the customer. The configuration will be 
completed when all the questions have been answered and confirmed.

##### Terminology

- **Catalog**: The collection of articles, question-sets and questions which can be combined to form a configurable product;
- **Article**: A purchasable item which can contains retail information (price, sku, ean, etc);
- **QuestionSet**: A named container for questions, this set describes the base question;
- **Question**: A member of a question-set, multiple questions can relate to a single QuestionSet;
- **Modifier**: A configuration modifier that will be triggered if the configuration meets it's criteria;
- **Product**: A combined set catalog entities that together form a configurable product;
- **Configuration**: A started or completed configuration of a product, responsible for keeping track of the answers and the related actions;

##### Example
```
------------------------------------------------------------------------------------------
Lets say we have a couple of catalog items:
------------------------------------------------------------------------------------------

- Article: ColorGreen
- Article: ColorRed
- Article: ColorOrange
- Article: ColorBlue

- Question: Name [option]
- Question: Street [input]
- Question: Address [input]
- Question: Country [option]
- Question: Color [Article<ColorGreen>, Article<ColorRed>]
- Question: Color [Article<ArticleOrange>, Article<ColorBlue>]
- Question: Radial <Yes> <No>
- Question: Length [input]
- Question: Width [input]
- Question: Height [input]

- QuestionSet: We need some personal information
    - Question: Name [option]
    - Question: Street [input]
    - Question: Address [input]
    - Question: Country [option]

- QuestionSet: What how long should it be?
    - Question: Length [input]

- QuestionSet: What are the dimensons?
    - Question: Length [input]
    - Question: Width [input]
    - Question: Height [input]

- QuestionSet: What colour should it be?
    - Question: Color [Article<ColorGreen>, Article<ColorRed>]

- QuestionSet: What colour should the rails be?
    - Question: Color [Article<ArticleOrange>, Article<ColorBlue>]

- QuestionSet: Would you like Child Safety?
    - Question: Radial <Yes> <No>

------------------------------------------------------------------------------------------
With this you can build a product:
------------------------------------------------------------------------------------------

- ProductA: A simple product only requires a the dimensions and a single color:
    - QuestionSet: What are the dimensons?
        - Question: Length [input]
        - Question: Width [input]
        - Question: Height [input]
    - QuestionSet: What colour should it be?
        - Question: Color [Article<ColorGreen>, Article<ColorRed>]

- ProductB: A simple product but has multiple color choices requiring a width and has a with child safety option:
    - QuestionSet: What colour should it be?
        - Question: Color [Article<ColorGreen>, Article<ColorRed>]
    - QuestionSet: What colour should the rails be?
        - Question: Color [Article<ArticleOrange>, Article<ColorBlue>]
    - QuestionSet: What how long should it be?
        - Question: Length [input]
    - QuestionSet: Would you like Child Safety?
        - Question: Radial <Yes> <No>

- ProductC: Try out a sample product, it's free! It requires some personal information though.
    - QuestionSet: What colour should it be?
        - Question: Color [Article<ColorGreen>, Article<ColorRed>]
    - QuestionSet: We need some personal information
        - Question: Name [option]
        - Question: Street [input]
        - Question: Address [input]
        - Question: Country [option]

------------------------------------------------------------------------------------------
You have now some configurable products, so the customer can start a new configuration
and answer the question to configure it to their needs. But that's not all, we still
have the modifiers. Currently only price-modifiers are added.
------------------------------------------------------------------------------------------

When the article is selected in the target question and the question with the length is answered,
if a modifier match is found it will be applied.

- Modifier A:
    - Article: Article A, unit_quantity: 100 unit_type: CM (price per 100 cm)
    - Question Article: Target <Article Choice>
    - Question Length: Trigger <150>
    - Modifier: Partial > Pay for what is used > 150cm

- Modifier B:
    - Article: Article A, unit_quantity: 100 unit_type: CM (price per 100 cm)
    - Question Article: Target <Article Choice>
    - Question Length: Trigger <150>
    - Modifier: Partial > Pay for how many unit_types is required to produce > 200 cm
```
