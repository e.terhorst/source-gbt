from django.contrib.contenttypes.models import ContentType
from dynamic_fixtures.fixtures import BaseFixture

from gbt_catalog.factories.question import (
    QuestionInputFieldFactory,
    QuestionSetFactory,
    QuestionArticleChoiceFieldFactory
)
from gbt_catalog.models.question import Question, QuestionToSet


class Fixture(BaseFixture):

    @staticmethod
    def _create_question(generic_questions):
        questions = {}
        for reference, question in generic_questions.items():
            content_type = ContentType.objects.get_for_model(question)
            question, _ = Question.objects.get_or_create(
                reference=reference,
                content_type=content_type,
                object_id=question.id
            )
            questions.update({reference: question})
        return questions

    @staticmethod
    def _create_question_set(question_sets):
        for reference, context in question_sets.items():
            question_set = QuestionSetFactory(
                reference=reference,
                title=context['title']
            )

            for priority, question in enumerate(context['questions']):
                QuestionToSet.objects.create(question_set=question_set, question=question, priority=priority)

    def load(self):
        questions = {
            "q_rail_of_roede": QuestionArticleChoiceFieldFactory(article_choices=[
                "Roede",
                "Rail",
            ]),
            "q_rail_roede_lengte": QuestionInputFieldFactory(input_field={
                "widget": {
                    "type": "integer_field",
                    "placeholder": "Bv. 120",
                    "label": "Lengte",
                    "help_text": "Lengte in centimeters"
                },
                "validation": {
                    "min": "20",
                    "max": "400",
                    "required": True,
                }
            }),
            "q_target_room": QuestionInputFieldFactory(input_field={
                "widget": {
                    "type": "text_field",
                    "label": "Ruimte",
                    "placeholder": "Bv. Woonkamer",
                    "help_text": None,
                },
                "validation": {
                    "min_length": 3,
                    "max_length": 64,
                    "required": True
                }
            }),
            "q_naw_naam": QuestionInputFieldFactory(input_field={
                "widget": {
                    "type": "text_field",
                    "label": "Naam",
                    "placeholder": "Bv. Lau Schroeter",
                    "help_text": "Voor en achternaam"
                },
                "validation": {
                    "min_length": 3,
                    "max_length": 64,
                    "required": True
                }
            }),
            "q_naw_adres": QuestionInputFieldFactory(input_field={
                "widget": {
                    "type": "text_field",
                    "label": "Adres",
                    "placeholder": "Bv. Ondergrondseweg 123A",
                    "help_text": "Straat en huisnummer"
                },
                "validation": {
                    "min_length": 3,
                    "max_length": 64,
                    "required": True
                }
            }),
            "q_naw_postcode": QuestionInputFieldFactory(input_field={
                "widget": {
                    "type": "text_field",
                    "label": "Postcode",
                    "placeholder": "Bv. 1234 AA",
                    "help_text": None
                },
                "validation": {
                    "min_length": 3,
                    "max_length": 64,
                    "required": True
                }
            }),
            "q_naw_woonplaats": QuestionInputFieldFactory(input_field={
                "widget": {
                    "type": "text_field",
                    "label": "Woonplaats",
                    "placeholder": "Bv. Hemelen",
                    "help_text": None
                },
                "validation": {
                    "min_length": 3,
                    "max_length": 64,
                    "required": True
                }
            }),
        }
        questions = self._create_question(generic_questions=questions)

        # Note that 'questions' are in order of priority
        question_sets = {
            "qs_rail_of_roede": {
                "title": "Wenst u een rail of een roede?",
                "questions": [
                    questions['q_rail_of_roede']
                ]
            },
            "qs_rail_roede_lengte": {
                "title": "Wat is de gewenste lengte?",
                "questions": [
                    questions['q_rail_roede_lengte']
                ]
            },
            "qs_target_room": {
                "title": "Waar gaat het product geplaatst worden?",
                "questions": [
                    questions['q_target_room']
                ]
            },
            "qs_naw_gegevens": {
                "title": "Factuuradres",
                "questions": [
                    questions['q_naw_naam'],
                    questions['q_naw_adres'],
                    questions['q_naw_postcode'],
                    questions['q_naw_woonplaats']
                ]
            }
        }
        self._create_question_set(question_sets=question_sets)
