# Generated by Django 3.1.7 on 2021-03-04 12:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sku', models.CharField(db_index=True, max_length=4, unique=True)),
                ('ean', models.CharField(db_index=True, max_length=13, unique=True)),
                ('name', models.CharField(db_index=True, max_length=64)),
                ('description', models.TextField()),
                ('unit_type', models.CharField(max_length=32)),
                ('unit_quantity', models.DecimalField(decimal_places=2, max_digits=6)),
                ('unit_description', models.CharField(max_length=128)),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=6)),
            ],
            options={
                'verbose_name': 'Article',
                'verbose_name_plural': 'Articles',
                'db_table': 'gbt_catalog_article',
            },
        ),
        migrations.CreateModel(
            name='Modifier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reference', models.CharField(db_index=True, max_length=32, unique=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reference', models.CharField(max_length=64)),
                ('name', models.CharField(max_length=64)),
                ('description', models.TextField()),
                ('is_visible', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'Product',
                'verbose_name_plural': 'Products',
                'db_table': 'gbt_catalog_product',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reference', models.CharField(db_index=True, max_length=64)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype')),
            ],
            options={
                'verbose_name': 'Question',
                'verbose_name_plural': 'Questions',
                'db_table': 'gbt_catalog_question',
            },
        ),
        migrations.CreateModel(
            name='QuestionInputField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('input_field', models.JSONField()),
            ],
            options={
                'verbose_name': 'Question: InputField',
                'verbose_name_plural': 'Questions: InputFields',
                'db_table': 'gbt_catalog_question_input_field',
            },
        ),
        migrations.CreateModel(
            name='QuestionSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('reference', models.CharField(max_length=32)),
                ('title', models.CharField(max_length=128)),
            ],
            options={
                'verbose_name': 'Question set',
                'verbose_name_plural': 'Question sets',
                'db_table': 'gbt_catalog_question_set',
            },
        ),
        migrations.CreateModel(
            name='QuestionToSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('priority', models.PositiveIntegerField()),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='questions_from_set', to='gbt_catalog.question')),
                ('question_set', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='questions_from_set', to='gbt_catalog.questionset')),
            ],
            options={
                'db_table': 'gbt_catalog_rel_question_questionset',
                'ordering': ['priority', 'question_set'],
            },
        ),
        migrations.CreateModel(
            name='QuestionArticleChoiceField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('article_choices', models.ManyToManyField(related_name='choices', to='gbt_catalog.Article')),
            ],
            options={
                'verbose_name': 'Question: ArticleChoice',
                'verbose_name_plural': 'Questions: ArticleChoices',
                'db_table': 'gbt_catalog_question_article_choice',
            },
        ),
        migrations.CreateModel(
            name='ProductQuestionSet',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('priority', models.PositiveIntegerField()),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='question_sets', to='gbt_catalog.product')),
                ('question_set', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='gbt_catalog.questionset')),
            ],
            options={
                'db_table': 'gbt_catalog_rel_product_questionset',
                'ordering': ['priority', 'question_set'],
            },
        ),
        migrations.CreateModel(
            name='ProductModifier',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modifiers', models.ManyToManyField(to='gbt_catalog.Modifier')),
                ('product', models.OneToOneField(on_delete=django.db.models.deletion.PROTECT, related_name='modifier_set', to='gbt_catalog.product')),
            ],
        ),
        migrations.CreateModel(
            name='ModifierPriceArticleQuestion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modifier_method', models.CharField(choices=[('ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL', 'Decimal(answer.value/article.unit_type_quantity) * price'), ('ANSWER_VALUE_TO_UNIT_TYPE_FULL', 'math.ceil(anwer.value/article.unit_type_quantity) * price')], max_length=128)),
                ('article', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='gbt_catalog.article')),
                ('question_target', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='target', to='gbt_catalog.question')),
                ('question_trigger', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='trigger', to='gbt_catalog.question')),
            ],
            options={
                'verbose_name': 'Modifier price: A>Q',
                'verbose_name_plural': 'Modifiers Price: A>Q',
                'db_table': 'gbt_catalog_modifier_price_article_question',
            },
        ),
    ]
