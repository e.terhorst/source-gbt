import factory

from gbt_configuration.factories.configuration import ConfigurationFactory
from gbt_configuration.models.answer import Answer


class AnswerFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Answer
        django_get_or_create = ('configuration', )

    configuration = factory.SubFactory(ConfigurationFactory)
    answers = {}
