from dynamic_fixtures.fixtures import BaseFixture

from gbt_catalog.factories.product import ProductFactory, ProductQuestionSetFactory
from gbt_catalog.models.modifier import Modifier
from gbt_catalog.models.product import ProductModifier


class Fixture(BaseFixture):

    dependencies = [
        ('gbt_catalog', '0003_modifiers'),
    ]

    @staticmethod
    def _create_products(products):
        for product, context in products.items():
            for priority, question_set_reference in enumerate(context['question_sets']):
                ProductQuestionSetFactory(
                    product=product,
                    question_set__reference=question_set_reference,
                    priority=priority
                )

            product_modifier, created = ProductModifier.objects.get_or_create(product=product)
            if created:
                modifiers = Modifier.objects.filter(reference__in=context['modifiers'])
                product_modifier.modifiers.set(modifiers)

    def load(self):
        products = {
            ProductFactory(
                name='Roede of rails',
                reference='roede-of-rails',
                description='Snel en eenvoudig uw gordijnen ophangen? Met een roede of een rail '
                            'is dat snel geregeld.',
                is_visible=True
            ): {
                'question_sets': [
                    'qs_rail_of_roede',
                    'qs_rail_roede_lengte',
                    'qs_target_room',
                    'qs_naw_gegevens'
                ],
                'modifiers': [
                    'mp_0001',
                    'mp_0002',
                ]
            }
        }

        self._create_products(products=products)
