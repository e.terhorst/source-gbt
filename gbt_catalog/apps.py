from django.apps import AppConfig


class CatalogConfig(AppConfig):
    name = 'gbt_catalog'
    verbose_name = 'catalogs'
