from rest_framework import viewsets

from gbt_catalog.models.product import Product
from gbt_catalog.serializers.product import ProductListSerializer


class ProductViewSet(viewsets.ModelViewSet):

    queryset = Product.objects.visible()
    serializer_class = ProductListSerializer
    http_method_names = ('get', )
