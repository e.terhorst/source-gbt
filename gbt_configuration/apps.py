from django.apps import AppConfig


class ConfigurationConfig(AppConfig):
    name = 'gbt_configuration'
    verbose_name = 'configurations'
