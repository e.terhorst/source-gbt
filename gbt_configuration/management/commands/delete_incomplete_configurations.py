import logging

from django.core.management import BaseCommand

from gbt_configuration.models import Configuration

logger = logging.getLogger('django')


class Command(BaseCommand):
    """
    Temporary management command to remove incomplete configurations.
    Should be moved to a scheduled task system (e.g. Celery)
    """

    def handle(self, *args, **kwargs):
        incomplete_configurations = Configuration.objects.incomplete()
        logger.info(f'Deleting {incomplete_configurations.count()} configurations')
        incomplete_configurations.delete()
