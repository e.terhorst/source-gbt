from django.utils.translation import ugettext as _
from django.contrib import admin

from gbt_catalog.models.modifier import Modifier, ModifierPriceArticleQuestion


@admin.register(Modifier)
class ArticleAdmin(admin.ModelAdmin):

    list_display = ('id', 'reference', 'content_object', 'object_id')
    list_display_links = ('id', 'reference', )
    search_fields = ('id', 'reference', )
    ordering = ('reference',)
    readonly_fields = ('id', )

    fieldsets = (
        (_('Public context'), {
            'fields': ('object_id',)
        }),
        (_('Management context'), {
            'fields': ('reference', )
        }),
    )


@admin.register(ModifierPriceArticleQuestion)
class ModifierPriceArticleQuestion(admin.ModelAdmin):

    list_display = ('id', 'article', 'question_trigger', 'question_target', 'modifier_method',)
    list_display_links = ('id', )
    search_fields = ('id', 'article',)
    list_filter = ('modifier_method', )
    ordering = ('id',)
    readonly_fields = ('id', )

    fieldsets = (
        (_('Public context'), {
            'fields': ('article', 'question_trigger', 'question_target', 'modifier_method', )
        }),
        (_('Management context'), {
            'fields': ('id', )
        }),
    )
