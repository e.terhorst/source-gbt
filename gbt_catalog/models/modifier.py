from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _

from gbt_catalog.models.article import Article
from gbt_catalog.models.question import Question


class Modifier(models.Model):

    reference = models.CharField(max_length=32, unique=True, db_index=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class ModifierPriceArticleQuestion(models.Model):
    # TODO: Reduce name to ModifierPrice > For now it's always artice/question bound
    """
    Modifier that is related to a question and an article combination.
    The method defines what function should be used to form the final price.
    """

    """
    Global product information for math examples
    Answer:
        value: 150
    Article:
        price: 11.50
        unit_type: cm
        unit_type_quantity: 100
    """

    METHOD_ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL = "ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL"

    METHOD_ANSWER_VALUE_TO_UNIT_TYPE_FULL = "ANSWER_VALUE_TO_UNIT_TYPE_FULL"

    METHOD_CHOICES = (
        (METHOD_ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL, _("Decimal(answer.value/article.unit_type_quantity) * price")),
        (METHOD_ANSWER_VALUE_TO_UNIT_TYPE_FULL, _("math.ceil(anwer.value/article.unit_type_quantity) * price"))
    )

    article = models.ForeignKey(Article, on_delete=models.PROTECT)
    question_trigger = models.ForeignKey(Question, related_name='trigger', on_delete=models.PROTECT)
    question_target = models.ForeignKey(Question, related_name='target', on_delete=models.PROTECT)

    # TODO: remove modifier_ prefix
    modifier_method = models.CharField(max_length=128, choices=METHOD_CHOICES)

    class Meta:
        db_table = 'gbt_catalog_modifier_price_article_question'
        verbose_name = _('Modifier price: A>Q')
        verbose_name_plural = _('Modifiers Price: A>Q')

    def __str__(self):
        return f'{self.modifier_method}: {self.question_trigger.reference} <> {self.question_target.reference}'
