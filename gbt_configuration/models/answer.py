from django.db import models
from django.utils.translation import ugettext_lazy as _

from gbt_configuration.models import Configuration


class Answer(models.Model):

    configuration = models.OneToOneField(Configuration, related_name='answers', on_delete=models.PROTECT)
    answers = models.JSONField()  # TODO: rename to json (makes it: configurator.answers.json)

    updated = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'gbt_configuration_answer'
        verbose_name = _('Answer')
        verbose_name_plural = _('Answers')
