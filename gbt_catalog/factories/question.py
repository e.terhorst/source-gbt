import factory

from gbt_catalog.models.article import Article
from gbt_catalog.models.question import (
    QuestionSet,
    QuestionInputField,
    QuestionArticleChoiceField
)


class QuestionSetFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = QuestionSet
        django_get_or_create = ('reference', )

    reference = factory.sequence(lambda n: "QS_{:04d}".format(n))
    title = factory.Faker('sentence')


class QuestionInputFieldFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = QuestionInputField
        django_get_or_create = ('input_field',)

    input_field = {
        "type": "charField",
        "widget": {
            "type": "text",
            "placeholder": "Something.."
        }
    }


class QuestionArticleChoiceFieldFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = QuestionArticleChoiceField

    @factory.post_generation
    def article_choices(self, create, extracted, **kwargs):
        if not create:
            return  # Simple build, do nothing.

        if extracted:
            for article_choice_name in extracted:
                article = Article.objects.get(name=article_choice_name)
                self.article_choices.add(article)
