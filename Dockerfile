FROM python:3.9.2-slim-buster

WORKDIR /app/

ENV PYTHONUNBUFFERED=1 \
    POETRY_VIRTUALENVS_CREATE=false

# Update and install OS dependencies
RUN apt-get update && apt-get install -y \
    curl \
    build-essential \
    python3-psycopg2 \
    postgresql-contrib \
    libpq-dev

# Install the Poetry package manager
RUN pip3 install --no-cache poetry
COPY pyproject.toml poetry.lock /app/
RUN poetry install --no-root --no-dev

EXPOSE 8000
