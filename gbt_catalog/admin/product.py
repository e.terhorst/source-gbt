from django.utils.translation import ugettext as _
from django.contrib import admin

from gbt_catalog.models.product import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):

    list_display = ('id', 'reference', 'name', 'is_visible')
    list_filter = ('is_visible',)
    list_display_links = ('id', 'reference', )
    search_fields = ('reference', 'name', )
    ordering = ('id',)

    fieldsets = (
        (_('Public context'), {
            'fields': ('name', 'description',)
        }),
        (_('Management context'), {
            'fields': ('reference', 'is_visible', )
        }),
    )
