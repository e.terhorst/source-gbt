
class ConfigurationServiceError(Exception):
    """
    Should be used as baseclass for product configuration errors.
    """
    default_code = 'configuration_error'
    default_detail = 'Unspecified product configuration error'


class NotAllQuestionsAnsweredConfigurationServiceError(ConfigurationServiceError):
    """
    Should be reaised when attempting to perform an action that requires all questions
    related to the configuration to be answered.
    """
    default_code = 'not_all_questions_answered'
    default_detail = 'Cannot perform action on product configuration unless all questions are answered'


class AlreadyCompletedConfigurationServiceError(ConfigurationServiceError):
    """
    Should be raised when attempting to perform an action that requires the configuration
    to be in an incomplete (not final) state .
    """
    default_code = 'configuration_already_completed'
    default_detail = 'Cannot perform action on completed product configuration'


class ModifierMethodNotFoundConfigurationServiceError(ConfigurationServiceError):
    """
    Should be raised when attepmting to apply a modifier but the service related to the
    modifier does not exist.
    """
    default_code = 'modifier_not_found'
    default_detail = 'Cannot apply modifier, handler service not found'
