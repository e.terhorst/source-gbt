from django.utils.translation import ugettext as _
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from gbt_users.models import User


@admin.register(User)
class UserAdmin(UserAdmin):

    list_display = ('id', 'email', 'is_active', 'date_joined', 'last_login',
                    'first_name', 'last_name', 'is_staff',)
    list_filter = ('is_active', 'is_staff', 'is_superuser',)
    list_display_links = ('id', 'email',)
    search_fields = ('email', 'first_name', 'last_name',)
    readonly_fields = ('date_joined', 'last_login',)
    ordering = ('id',)

    fieldsets = (
        (_('Login credentials'), {
            'fields': ('email', 'password')
        }),
        (_('Personal info'), {
            'fields': ('first_name', 'last_name')
        }),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')
        }),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined')
        }),
    )
    add_fieldsets = fieldsets[:3]
