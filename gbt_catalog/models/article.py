from django.db import models
from django.utils.translation import ugettext as _


class Article(models.Model):
    """
    Description of a purchasable article.
    """

    sku = models.CharField(max_length=4, unique=True, db_index=True)
    ean = models.CharField(max_length=13, unique=True, db_index=True)

    name = models.CharField(max_length=64, db_index=True)
    description = models.TextField()

    # TODO: Make UnitType meta generic for reusability (catalog concept)
    unit_type = models.CharField(max_length=32)
    unit_quantity = models.DecimalField(max_digits=6, decimal_places=2)
    unit_description = models.CharField(max_length=128)
    unit_price = models.DecimalField(max_digits=6, decimal_places=2)

    class Meta:
        db_table = 'gbt_catalog_article'
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')

    def __str__(self):
        return f'{self.name} ({self.sku})'
