import gbt_configuration.services.question_set
import gbt_configuration.services.modifier


def build_for_configuration(configuration):
    """
    Build the answers storage dict for the configuration based on the chosen product.
    Note: Results will be stored as JSON in the database.

    :param gbt_configuration.models.Configuration configuration:
        The configuration to generate the dict structure for.

    :rtype: dict
    :return: Dictionary of the answer storage layout.
    """

    # Result is ordered by priority
    question_sets = gbt_configuration.services.question_set.all_for_configuration(
        configuration=configuration
    )

    answer_tree = {}

    for product_question_set in question_sets:
        question_set = product_question_set.question_set
        answer_tree[question_set.reference] = {}

        question_to_sets = product_question_set\
            .question_set\
            .questions_from_set\
            .prefetch_related('question')\
            .all()

        for question_to_set_relation in question_to_sets:
            question = question_to_set_relation.question
            answer_tree[question_set.reference][question.reference] = None

    return answer_tree


def validate_answers(configuration, qs_reference, answers):
    # TODO: Should validate questions based on their type (Article, Char) and their behavior (e.g. widget context)
    pass


def update_answers_set(configuration, answers):
    """
    A list of answers from multiple question sets that have changed should be re-evaluated and submitted.
    Note: This is curently used for admin manual json-update, eventually admin support should be embedded within
          a client that follows the ReST API flow. (eat-your-own-dogfood principle)

    :param gbt_configuration.models.configuration configuration: Handle new answers for given configuration instance.
    :param dict answers: Dictionairy with answers of multiple question sets.

    :rtype: gbt_configuration.models.Configuration
    :return: Configuration for which the answers have been updated.
    """

    for qs_reference, qs_answers in answers.items():
        handle_answers(configuration=configuration, qs_reference=qs_reference, answers_update=qs_answers)

    return configuration


def handle_answers(configuration, qs_reference, answers_update):
    """
    Initiate the process of updating a new ansers for the given questionset.

    :param gbt_configuration.models.configuration configuration: Handle new answers for given configuration instance.
    :param qs_reference: The new answers are for this given questionset with this reference.
    :param answers_update: The questionset answers that require to be handled.

    :rtype: gbt_configuration.models.configuration
    :return: Updated configuration instance.
    """

    modifiers_to_apply = gbt_configuration.services.modifier.get_for_answers_update(
        configuration=configuration,
        answers_update=answers_update
    )

    if modifiers_to_apply:
        gbt_configuration.services.modifier.apply(configuration=configuration,
                                                  modifiers_to_apply=modifiers_to_apply)

    configuration.answers.answers = update_answers(
        configuration=configuration,
        qs_reference=qs_reference,
        answers_update=answers_update
    )
    configuration.answers.save()

    return configuration


def update_answers(configuration, qs_reference, answers_update):
    """
    Update the new set of answers to the configuration's answers that have already been answered.

    :param gbt_configuration.models.configuration configuration: Configuration to set answers for.
    :param qs_reference: Update the answers are for the questionset with this reference.
    :param answers_update: The set of answers that have been submitted.

    :rtype: dict
    :return: Original answers updated with the updated answers.
    """

    answers = configuration.answers.answers
    answers[qs_reference].update(answers_update)

    return answers
