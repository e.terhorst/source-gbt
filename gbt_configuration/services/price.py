import math
from decimal import Decimal, ROUND_HALF_UP

from gbt_catalog.models.modifier import ModifierPriceArticleQuestion
from gbt_configuration.services.exceptions import ModifierMethodNotFoundConfigurationServiceError


def calculate_price_for_method(method, article, answer_value):
    """
    Calculates the price with the given method
    # TODO: Type checking on input, currently only Question/Article is possible so should not be an issue for now.
    """
    handlers = {
        ModifierPriceArticleQuestion.METHOD_ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL: _answer_value_to_unit_type_partial,
        ModifierPriceArticleQuestion.METHOD_ANSWER_VALUE_TO_UNIT_TYPE_FULL: _answer_value_to_unit_type_full
    }

    try:
        handler = handlers[method]
    except KeyError:
        raise ModifierMethodNotFoundConfigurationServiceError(
            f"Could not apply modifier '{method}', handler is not available."
        )
    else:
        return handler(article=article, answer_value=answer_value)


def _answer_value_to_unit_type_partial(article, answer_value):
    """
    Charge partial price: only charge what is actually used.  If the article's unit_type_quantity is 100
                          and the ordered product is 150 cm, the price is calculated for 1.5 units.

    Decimal(answer.value/article.unit_type_quantity) * price = final price
    Decimal(150/100) * Decimal(11.5) = Decimal(17.25)
          1.5        *     11.50     =      17.25
    """
    return Decimal(answer_value / article.unit_quantity) * article.unit_price


def _answer_value_to_unit_type_full(article, answer_value):
    """
    Charge full price: every exceedance of the article's unit_type_quantity is fully charged.
                       If the article unit_type_quantity is 100 cm and the ordered pruduct is 150 cm,
                       it will require two units to produce the requested size.

    math.ceil(answer.value/article.unit_type_quantity) * price = final price
    math.ceil(150/100) * Decimal(11.5) = Decimal(23.00)
           2                 11.50     =      23.00
    """
    return math.ceil(answer_value / article.unit_quantity) * article.unit_price


def get_for_configuration(configuration):
    return configuration.price.quantize(Decimal('0.01'), rounding=ROUND_HALF_UP)
