from django.contrib.contenttypes.models import ContentType
from dynamic_fixtures.fixtures import BaseFixture

from gbt_catalog.factories.modifiers import ModifierPriceQuestionArticleFactory
from gbt_catalog.models.modifier import ModifierPriceArticleQuestion, Modifier
from gbt_catalog.models.question import Question


class Fixture(BaseFixture):

    dependencies = [
        ('gbt_catalog', '0002_questions'),
    ]

    @staticmethod
    def _create_price_modifiers(price_modifiers):
        for reference, modifier in price_modifiers.items():
            content_type = ContentType.objects.get_for_model(modifier)
            Modifier.objects.get_or_create(reference=reference, content_type=content_type, object_id=modifier.id)

    def load(self):
        price_modifiers = {
            'mp_0001': ModifierPriceQuestionArticleFactory(
                article__sku='0001',
                question_trigger=Question.objects.get(reference='q_rail_roede_lengte'),
                question_target=Question.objects.get(reference='q_rail_of_roede'),
                modifier_method=ModifierPriceArticleQuestion.METHOD_ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL
            ),
            'mp_0002': ModifierPriceQuestionArticleFactory(
                article__sku='0002',
                question_trigger=Question.objects.get(reference='q_rail_roede_lengte'),
                question_target=Question.objects.get(reference='q_rail_of_roede'),
                modifier_method=ModifierPriceArticleQuestion.METHOD_ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL
            )
        }
        self._create_price_modifiers(price_modifiers=price_modifiers)
