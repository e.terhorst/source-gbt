from rest_framework import serializers

from gbt_catalog.models.product import Product, ProductQuestionSet
from gbt_catalog.serializers.question import QuestionSetSerializer


class ProductListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = (
            'name',
            'reference',
            'description',
        )
        read_only_fields = fields


class ProductQuestionSetSerializer(serializers.ModelSerializer):

    question_set = QuestionSetSerializer(read_only=True)

    class Meta:
        model = ProductQuestionSet
        fields = (
            'question_set',
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance=instance)
        return representation['question_set']
