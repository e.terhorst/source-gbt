import factory

from gbt_catalog.factories import ArticleFactory, QuestionInputFieldFactory
from gbt_catalog.models.modifier import ModifierPriceArticleQuestion


class ModifierPriceQuestionArticleFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = ModifierPriceArticleQuestion
        django_get_or_create = ('article', 'question_trigger', 'question_target',)

    article = factory.SubFactory(ArticleFactory)
    question_trigger = factory.SubFactory(QuestionInputFieldFactory)
    question_target = factory.SubFactory(QuestionInputFieldFactory)

    modifier_method = ModifierPriceArticleQuestion.METHOD_ANSWER_VALUE_TO_UNIT_TYPE_PARTIAL
