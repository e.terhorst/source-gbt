import factory
from django.utils.text import slugify

from gbt_catalog.factories.question import QuestionSetFactory
from gbt_catalog.models.product import Product, ProductQuestionSet


class ProductFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Product
        django_get_or_create = ('reference', )

    reference = factory.LazyAttribute(lambda o: slugify(o.name))

    name = factory.Faker('word')
    description = factory.Faker('words')

    is_visible = True


class ProductQuestionSetFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = ProductQuestionSet
        django_get_or_create = ('product', 'question_set',)

    product = factory.SubFactory(ProductFactory)
    question_set = factory.SubFactory(QuestionSetFactory)
    priority = factory.sequence(lambda n: n)
