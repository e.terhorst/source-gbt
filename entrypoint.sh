#!/usr/bin/env bash

# Set execute permissions.
chmod +x wait-for-command.sh

# Time to wait for database to become available.
WAIT_TIME=15

sh wait-for-command.sh --status 0 --time "$WAIT_TIME" --command "nc -z $DATABASE_HOST 5432"

# Exit script if any statements return non-true from this point on
set -e

if [ '1' = $? ]; then
    echo "Entrypoint: database on $DATABASE_PORT:5432 not ready within $WAIT_TIME seconds - aborting"
    exit 1
fi

# Initialize project if this has not yet been done.
if [ '0' != `psql -lqt -U "$DATABASE_USER" -h "$DATABASE_HOST" -c "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'public';" | sed 's/ //g'` ]; then
    echo "Entrypoint: application already initialized - skipping"
else
    echo "Entrypoint: application not yet initialized - initializing"
    ./manage.py load_dynamic_fixtures
fi

# Continue with original command
exec "$@"
