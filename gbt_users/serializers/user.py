from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model, password_validation
from rest_framework import serializers


class UserListSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = (
            'email',
            'first_name',
            'last_name',
            'is_active',
        )
        read_only_fields = fields


class UserRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = get_user_model()
        fields = (
            'email',
            'first_name',
            'last_name',
            'is_active',
            'date_joined',
            'last_login',
        )
        read_only_fields = fields


class UserCreateSerializer(serializers.ModelSerializer):

    password = serializers.CharField(label=_("Password"), max_length=32, write_only=True)
    password_confirm = serializers.CharField(label=_("Confirm password"), max_length=32, write_only=True)

    class Meta:
        model = get_user_model()
        fields = (
            'email',
            'first_name',
            'last_name',
            'password',
            'password_confirm'
        )

    def validate_password(self, value):
        """
        Validate password with Django's default password validator.

        :param str value: serializer's password field value
        :raises django.core.exceptions.ValidationError: re-raise of django's password validation errors.
        :rtype: str
        :return: Validated password field value
        """
        password_validation.validate_password(password=value)
        return value

    def validate(self, data):
        """
        Validate data that is based on multiple fields.

        :param dict data: serializer's fields data
        :raise rest_framework.exceptions.ValidationError: Password and confirmation do not match.
        :rtype: dict
        :return: Validated fields data
        """
        password = data.get('password')
        password_confirm = data.get('password_confirm')

        if password != password_confirm:
            raise serializers.ValidationError({
                'password_confirm': _("The password and password confirmation fields do not match.")
            })

        return data

    def create(self, validated_data):
        """
        Perform creation of a new user and properly set the user's password.
        :param dict validated_data: Serializer's validated data.
        :rtype: gbt_users.models.User
        :return: Newly created user object
        """
        user = get_user_model()(
            email=validated_data.get('email'),
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name')
        )
        user.set_password(raw_password=validated_data.get('password'))
        user.save()
        return user
