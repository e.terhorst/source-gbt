from rest_framework import serializers

from gbt_catalog.models.question import (
    Question,
    QuestionSet,
    QuestionToSet,
    QuestionInputField,
    QuestionArticleChoiceField
)
from gbt_catalog.serializers.article import ArticleRetrieveSerializer


class QuestionInputFieldSerializer(serializers.ModelSerializer):

    input_field = serializers.JSONField()

    class Meta:
        model = QuestionInputField
        fields = ('input_field',)

    def to_representation(self, instance):
        representation = super().to_representation(instance=instance)
        return {
            'type': 'input_field',
            'field': representation['input_field']
        }


class QuestionArticleChoiceSerializer(serializers.ModelSerializer):

    article_choices = ArticleRetrieveSerializer(many=True)

    class Meta:
        model = QuestionArticleChoiceField
        fields = ('article_choices',)

    def to_representation(self, instance):
        representation = super().to_representation(instance=instance)
        return {
            'type': 'article_choice',
            'choices': representation['article_choices']
        }


class QuestionRetrieveSerializer(serializers.ModelSerializer):

    object_serializers_map = {
        QuestionInputField: QuestionInputFieldSerializer,
        QuestionArticleChoiceField: QuestionArticleChoiceSerializer,
    }

    content_object = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Question
        fields = (
            'content_object',
            'reference',
        )

    def get_content_object(self, instance):
        serializer = self.object_serializers_map[instance.content_object.__class__]
        serialized = serializer(instance.content_object)
        return serialized.data

    def to_representation(self, instance):
        """
        Reduce nesting
        """
        representation = super().to_representation(instance=instance)
        new_representation = {'reference': representation['reference']}
        new_representation.update({key: value for key, value in representation['content_object'].items()})
        return new_representation


class QuestionToSetSerializer(serializers.ModelSerializer):

    question = QuestionRetrieveSerializer()

    class Meta:
        model = QuestionToSet
        fields = (
            'question',
        )

    @staticmethod
    def get_question(obj):
        """
        Reduce nesting
        """
        serializer = QuestionRetrieveSerializer(obj.question).data
        return {
            'question': serializer['content_object']
        }


class QuestionSetSerializer(serializers.ModelSerializer):

    questions = QuestionToSetSerializer(many=True, source='questions_from_set')

    class Meta:
        model = QuestionSet
        fields = (
            'questions',
            'reference',
            'title',
        )

    def to_representation(self, instance):
        """
        Reduce nesting
        """
        representation = super().to_representation(instance=instance)
        return {
            'reference': representation['reference'],
            'title': representation['title'],
            'questions': [question['question'] for question in representation['questions']]
        }
